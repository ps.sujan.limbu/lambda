package stream;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
//        int[] test = {1,5,3,8,9,4};
//        IntStream intStream = Arrays.stream(test);
//        intStream.forEach(System.out::println);

        List<String> nameList = Arrays.asList("bir","dal","tek","vim","bom","tommy","kumar");
//        Stream<String> strStream = nameList.stream();

        //Iteration
        nameList.stream().forEach(name -> System.out.println(name));

        //Filtering
        Stream<String> val = nameList.stream().filter(name -> name.length()>3);
        val.forEach(s -> System.out.println(s+":)"));
        Stream<String> str = nameList.stream().filter(name -> name.contains("i"));
        str.forEach(s -> System.out.println(s+":("));
        Stream<String> filter = nameList.stream().filter(name -> name.startsWith("b")&&name.length()<4);
        filter.forEach(System.out::println);
        filter.close();

        //Mapping
        List<String> mapStr = nameList.stream().filter(name -> name.length()<=3)
                .map(name -> name+" bahadur").collect(Collectors.toList());
        System.out.println(mapStr);
        //flatmap
        List<String> tempName = Arrays.asList("tom","jerry");
        List<List<String>> listOfNameList = Arrays.asList(nameList, tempName);
        System.out.println("-----------------\n"+listOfNameList);
        List<String> flatMapList = listOfNameList.stream()
                .flatMap(alist -> alist.stream().map(name -> name+" bahadur"))
                .collect(Collectors.toList());
        System.out.println("Flat Map: "+flatMapList);

        //Matching
        boolean any = nameList.stream().anyMatch(name -> name.contains("x"));
        System.out.println("Any Match 'x': "+ any);
        boolean any2 = nameList.stream().anyMatch(name -> name.contains("i"));
        System.out.println("Any Match 'i': "+ any2);
        boolean all = nameList.stream().allMatch(name -> name.contains("i"));
        System.out.println("All Match 'i': "+ all);
        boolean none = nameList.stream().noneMatch(name -> name.contains("x"));
        System.out.println("none Match 'x': "+ none);

        //reduction,stream reduces to single resultant value
        Optional<String> finalName = nameList.stream().reduce((name1, name2)->name1+"."+name2);
        System.out.println("final name: "+finalName.get());
        Optional<String> reducedName = nameList.stream().filter(name -> name.length()<=3)
                .reduce((name1, name2)->name2+"."+name1);
        System.out.println("final name: "+reducedName.get());


        //Collecting
        //To string buffer
//        StringBuffer buildCollect = nameList.stream()
//                .collect(StringBuffer::new, (name1, name2) -> name1.append(name2), (x, r) -> x.append(r));
//        System.out.println(buildCollect);
        //To List
        List<String> collectList = nameList.stream()
                .map(name -> name.toUpperCase()).collect(Collectors.toList());
        System.out.println("Collect List: "+collectList);
        //To Map
        Map<String, String> collectMap = nameList.stream()
                .collect(Collectors.toMap(Function.identity(), name -> name.toUpperCase()));
        System.out.println("Collect Map: "+ collectMap);
        //collecting and then
        Set<String> setCollect = nameList.stream()
                .collect(Collectors.collectingAndThen(Collectors.toSet(), name -> Collections.unmodifiableSet(name)));
        System.out.println("Collect Set: "+setCollect);
        String joined = nameList.stream().collect(Collectors.joining("/"));
        System.out.println("Joined: "+ joined);


    }
}
