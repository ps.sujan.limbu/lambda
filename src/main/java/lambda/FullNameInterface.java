package lambda;

//single method interface
@FunctionalInterface
public interface FullNameInterface {
    String getFullName(String firstName, String lastName);
}
