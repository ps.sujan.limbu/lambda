package lambda;

import lambda.FullNameInterface;

public class LambdaDemo {
    public static void main(String[] args) {

        //Anonymous inner class Implementation
        FullNameInterface fullNameInterface = new FullNameInterface() {
            public String getFullName(String firstName, String lastName) {
                return firstName+" "+lastName;
            }
        };
        String fullName = fullNameInterface.getFullName("sujan","limbu");
        System.out.println("Full Name: "+fullName);

        //Lambda implementation
        FullNameInterface fullNameInterfaceLambda = (name, surName) -> { return name+" "+surName; };
        System.out.println("Full Name with Lambda: "+ fullNameInterfaceLambda.getFullName("dujan","jimbu"));
    }
}
