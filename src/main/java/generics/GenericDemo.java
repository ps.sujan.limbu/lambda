package generics;

class GenericDemo {
    public static void main(String[] args) {
        Data<Integer> integerData = new Data<>(5,4);
        integerData.printClass();
        System.out.println(integerData.getData1() + integerData.getData2());

        Data<String> stringData = new Data<>("sujan", "limbu");
        stringData.printClass();
        System.out.println(stringData.getData1()+" "+stringData.getData2());

        HelperClass.printFromGeneric(155);
        HelperClass.printFromGeneric("Sujan");
    }
}

class HelperClass{
    public static  <T> void printFromGeneric(T data){
        System.out.println(data);
    }
}


