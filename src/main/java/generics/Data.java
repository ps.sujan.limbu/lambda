package generics;

public class Data<T> {
    T data1;
    T data2;

    Data(T data1, T data2){
        this.data1 = data1;
        this.data2 = data2;
    }

    public T getData1() {
        return data1;
    }

    public T getData2() {
        return data2;
    }

    public void printClass(){
        System.out.println(data1.getClass()); ;
    }
}
